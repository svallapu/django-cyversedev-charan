/**
 * CvVerse/IGB Bridge
 * by Loraine Lab
 */

var igbIsRunning = false;
var igb_params = {};

$(window).on("load",function(e) {
	contactIgb();
});


// If IGB is running, open REST URL to IGB. 
// Otherwise, invite user to download and launch IGB.
function contactIgb() {
    var statusCheckUrl = 'http://127.0.0.1:7085/igbStatusCheck';
    var xhr = createCORSRequest('GET', statusCheckUrl);
    if (!xhr) {
        return;
    }
    xhr.onload = function() {
        if (xhr.status == 200) {
	    // IGB answered with response code 200 and JSON data
            igbIsRunning = true;
            $("#igbIsRunningBlock").removeClass("hide");
            $("#dataAreLoading").removeClass("hide");
	    loadData();
            //setTimeout(loadData(),3000);
        } else {
	    // IGB answered but response code not 200
            $("#igbIsNotRunningBlock").removeClass("hide");
	    igbIsRunning = false;
        }
    };
    xhr.onerror = function() {
	// IGB did not answer (is not running)
        $("#igbIsNotRunningBlock").removeClass("hide");
	igbIsRunning = false;
    };
    xhr.send();
}

function loadData() {
     	var query_elements = parseQuery(location.search.substring(1));
     	var server_url = query_elements['server_url'];
	makeAndOpenIgbUrl(server_url); // no need to query web service
}

function makeAndOpenIgbUrl(server_url) {
    var igb_url="http://127.0.0.1:7085/IGBControl?query_url="+server_url+'&cyverse_data=true';
    var xhr = createCORSRequest('GET', igb_url);
    if (!xhr) {
        return;
    }
    xhr.onload = function() {
        if (xhr.status == 204) {
	    // Note: Ann noticed that these calls sometimes fail to
	    // open data set, navigate to the gene, etc.
	    // Probably this is related to threading in IGB.
	    // If yes, we may want to send instructions to IGB in
	    // more than one URL, e.g., first load the genome,
	    // then go to a location, then open the data set, then
	    // load the residues, etc. Also, we need to make sure that
	    // IGB does not bother the user with any dialogs, e.g.,
	    // useless notifications to zoom in or accept a certificate.
            $("#dataAreLoading").addClass("hide");
            $("#dataLoaded").removeClass("hide");
        }
        else {
            $("#dataAreLoading").addClass("hide");
            $("#dataLoadingError").removeClass("hide");
        }
    };
    xhr.onerror = function() {
        $("#dataAreLoading").addClass("hide");
        $("#dataLoadingError").removeClass("hide");
    };
    console.log("Opening IGB Url: " + igb_url);
    xhr.send();
}


// Look up seqid, start, and end for the given geneId
// Add these values to global variable igb_params
function addCoordinatesForGeneId(geneId) {
    // Note: geneIdLookup.py is version-controlled in bioviz repository
    var gene_url = location.origin+"/cgi-bin/geneIdLookup.py?gene_id="+geneId;
    var xhr = createCORSRequest('GET', gene_url);
    if (!xhr) {
       console.log("Could not form request: "+ gene_url);
    }
    xhr.onload = function() {
        if (xhr.status=200){
            gene_coords = JSON.parse(this.response);
	    igb_params["start"]=parseInt(gene_coords["start"])-500;
	    igb_params["end"]=parseInt(gene_coords["end"])+500;
	    igb_params["seqid"]=gene_coords["seqid"];
            makeAndOpenIgbUrl();
        }
    };
    xhr.onerror = function(err) {
        console.log(err);
    };
    xhr.send();
}


// Create and return an XHR object.
function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest for IE.
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
}

// Split query string into key value pairs
function parseQuery(str) {
    var ret = {};
    $.each(str.split("&"), function() {
        var data = this.split('='),
            name = decodeURIComponent(data.shift()),
            val = decodeURIComponent(data.join("=")).replace('+', ' '),
            nameVal = name.match(/(.*)\[(.*)\]/);
        if (nameVal === null) {
            ret[name] = val;
        }
        else {
            name = nameVal[1];
            nameVal = nameVal[2];
            if (!ret[name]) {
                ret[name] = nameVal ? {} : [];
            }
            if ($.isPlainObject(ret[name])) {
                ret[name][nameVal] = val;
            }
            else if($.isArray(ret[name])){
                ret[name].push(val);
            }
        }
    });
    return ret;
}


