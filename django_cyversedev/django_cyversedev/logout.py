#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import json
import sys
from . import refreshToken
from . import RedisClient


def logout(sessionid):
    client = RedisClient()
    redis = client.conn

    accessToken = json.loads(redis.get(sessionid))['access_token'];
    redis.delete(accessToken)
    redis.delete(sessionid)

    redirectUrl = 'http://ec2-3-82-194-143.compute-1.amazonaws.com/'
    sys.stdout.write('Content-type: text/plain\n\n')
    sys.stdout.write(redirectUrl)


def getSession():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "sessionid":
                return arguments[key].value


def doIt():
    sessionid = getSession()
    logout(sessionid=sessionid)


doIt()
