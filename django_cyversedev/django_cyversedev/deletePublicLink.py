#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import json
import sys
from . import refreshToken
from . import RedisClient


def deletePublicLink(url, sessionid):
    client = RedisClient()
    redis = client.conn

    attempts = 0
    while attempts < 3:
        accessToken = json.loads(redis.get(sessionid))['access_token']
        username = json.loads(redis.get(accessToken))['result']['username']
        req_data = '{"username","public", "permission":"NONE"}'
        req_url = 'https://agave.iplantc.org/files/v2/pems/system/data.iplantcollaborative.org' + url
        r = requests.post(req_url, headers={'Authorization': 'Bearer ' + accessToken, 'cache-control': 'no-cache'},
                          data={"username": "public", "permission": "NONE"})
        json_data = json.loads(r.text)
        if "fault" not in json_data:
            break
        else:
            attempts += 1
            refreshToken.refreshToken(sessionId)

    sys.stdout.write('Content-type: application/json\n\n')
    sys.stdout.write(json.dumps(json_data['result']))


def getUrl():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "url":
                return arguments[key].value


def getSession():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "sessionid":
                return arguments[key].value


def doIt():
    url = getUrl()
    sessionid = getSession()
    deletePublicLink(url=url, sessionid=sessionid)


doIt()
