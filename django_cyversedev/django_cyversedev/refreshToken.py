#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import configparser
import json
import sys
import logging
import binascii
from . import RedisClient


def refreshToken(sessionid):
    client = RedisClient()
    redis = client.conn
    config = configparser.ConfigParser()
    config.read('config.cfg')
    consumerKey = config.get('DEFAULT', 'consumerKey')
    consumerSecret = config.get('DEFAULT', 'consumerSecret')
    refreshToken = json.loads(redis.get(sessionid))['refresh_token']
    r = requests.post('https://agave.iplantc.org/token',
                      headers={"content-type": "application/x-www-form-urlencoded", "cache-control": "no-cache"},
                      data={"grant_type": "refresh_token", "refresh_token": refreshToken, "client_id": consumerKey,
                            "client_secret": consumerSecret})
    json_data = json.loads(r.text)
    # Rename key accessToken and replace it with new which was used to store userdata.
    accessToken = json.loads(redis.get(sessionid))['access_token']
    redis.set(sessionid, json.dumps(json_data))
    newAccessToken = json.loads(redis.get(sessionid))['access_token']
    redis.rename(accessToken, newAccessToken)
