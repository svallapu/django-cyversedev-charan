#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import json
import sys


def getUserData(accessToken):
    r = requests.get('https://agave.iplantc.org/profiles/v2/me', headers={'Authorization': 'Bearer ' + accessToken})
    json_data = json.loads(r.text)
    return json_data
