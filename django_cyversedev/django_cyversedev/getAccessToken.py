#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import configparser
import json
import sys
import logging
import pdb
import binascii
from . import getUserData
from . import RedisClient


def getAccessToken():
    client = RedisClient()
    redis = client.conn

    authcode = getAuthCode()

    config = configparser.ConfigParser()
    config.read('config.cfg')
    consumerKey = config.get('DEFAULT', 'consumerKey')
    consumerSecret = config.get('DEFAULT', 'consumerSecret')
    callbackUrl = config.get('DEFAULT', 'callbackUrl')
    rooturl = config.get('DEFAULT', 'rooturl')
    r = requests.post('https://agave.iplantc.org/token',
                      data={'grant_type': 'authorization_code', 'code': authcode, 'redirect_uri': callbackUrl,
                            'client_id': consumerKey, 'client_secret': consumerSecret})
    json_data = json.loads(r.text)
    session_id = generateRandomSessionKey()
    redis.setex(session_id, 2600000, json.dumps(json_data))
    accessToken = json.loads(redis.get(session_id))['access_token']
    userdata = getUserData.getUserData(accessToken)

    redis.setex(accessToken, 2600000, json.dumps(userdata))
    # print json.loads(redis.get(accessToken))['result']['username']

    pastebin_url = rooturl + '/cyverseData.html' + '?sessionid=' + session_id;
    sys.stdout.write('Set-Cookie: sessionid=' + session_id + '\n');
    sys.stdout.write('Content-type: text/html\n\n')
    sys.stdout.write(pastebin_url)


def generateRandomSessionKey():
    return binascii.hexlify(os.urandom(16))


def getAuthCode():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "authCode":
                return arguments[key].value


def doIt():
    getAccessToken()


doIt()
