from django.shortcuts import render


def index(request):
    context = {}
    return render(request, 'index.html', context)


def home(request):
    context = {}
    return render(request, 'cyverseHome.html', context)

def loginCyverse(request):
    context = {}
    return render(request, 'loginCyverse.html', context)


def oauth(request):
    context = {}
    return render(request, 'oauth.html', context)
