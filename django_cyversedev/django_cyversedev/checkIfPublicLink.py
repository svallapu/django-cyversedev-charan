#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import json
import sys
from . import refreshToken
from . import RedisClient


def checkIfPublicLink(fileUrl, sessionid):
    client = RedisClient()
    redis = client.conn
    attempts = 0
    while attempts < 2:
        accessToken = json.loads(redis.get(sessionid))['access_token']
        username = json.loads(redis.get(accessToken))['result']['username']
        url = 'https://agave.iplantc.org/files/v2/pems/system/data.iplantcollaborative.org' + fileUrl + '?username=public&filter=permission'
        r = requests.get(url, headers={'Authorization': 'Bearer ' + accessToken, 'cache-control': 'no-cache'})
        json_data = json.loads(r.text)
        if 'fault' in r.text:
            attempts = attempts + 1
            refreshToken.refreshToken(sessionid)
        else:
            break

    sys.stdout.write('Content-type: application/json\n\n')
    sys.stdout.write(json.dumps(json_data['result'][0]["permission"]["read"]))


def getUrl():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "url":
                return arguments[key].value


def getSession():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "sessionid":
                return arguments[key].value


def doIt():
    fileUrl = getUrl()
    sessionid = getSession()
    checkIfPublicLink(fileUrl=fileUrl, sessionid=sessionid)


doIt()
