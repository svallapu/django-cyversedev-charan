#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import ConfigParser
import json
import sys
import refreshToken
from RedisClient import RedisClient


def getFileList(fileUrl, sessionid):
    client = RedisClient()
    redis = client.conn
    attempts = 0
    while attempts < 2:
        accessToken = json.loads(redis.get(sessionid))['access_token']
        username = json.loads(redis.get(accessToken))['result']['username']
        if not fileUrl:
            url = 'https://agave.iplantc.org/files/v2/listings/' + username + '/'
        else:
            url = 'https://agave.iplantc.org/files/v2/listings' + fileUrl
        r = requests.get(url, headers={'Authorization': 'Bearer ' + accessToken, 'cache-control': 'no-cache'})
        json_data = json.loads(r.text)
        if 'fault' in r.text:
            attempts = attempts + 1
            refreshToken.refreshToken(sessionid)
        else:
            break

    sys.stdout.write('Content-type: application/json\n\n')
    sys.stdout.write(json.dumps(json_data['result']))


def getUrl():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "url":
                return arguments[key].value


def getSession():
    arguments = cgi.FieldStorage()
    if arguments:
        for key in arguments.keys():
            if key == "sessionid":
                return arguments[key].value


def doIt():
    fileUrl = getUrl()
    sessionid = getSession()
    getFileList(fileUrl=fileUrl, sessionid=sessionid)


doIt()
