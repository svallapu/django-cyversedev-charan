#!/usr/bin/env python
import cgi
import os
import cgitb
import requests
import configparser
import sys
import json

config = configparser.ConfigParser()
config.read('config.cfg')
consumerKey=config.get('DEFAULT', 'consumerKey')
consumerSecret=config.get('DEFAULT','consumerSecret')
callbackUrl=config.get('DEFAULT', 'callbackUrl')
redirectURL= 'https://agave.iplantc.org/authorize/?client_id='+consumerKey+'&response_type=code&redirect_uri='+callbackUrl+'&scope=PRODUCTION&response_type=code&state=867'
sys.stdout.write( 'Content-type: text/plain\n\n')
sys.stdout.write(redirectURL)
