#!/usr/bin/env python

import redis
import configparser


class Singleton(type):
    """
    An metaclass for singleton purpose. Every singleton class should inherit from this class by 'metaclass=Singleton'.
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class RedisClient(object):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.cfg')
        redispassword = config.get('DEFAULT', 'redispassword')
        self.pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0, password=redispassword)

    @property
    def conn(self):
        if not hasattr(self, '_conn'):
            self.getConnection()
        return self._conn

    def getConnection(self):
        self._conn = redis.Redis(connection_pool=self.pool)
